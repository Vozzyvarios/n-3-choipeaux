import csv

with open("file://srv-dc01.d0440021j.fr/Perso/ELEVES/1ERE3/ABRASSART/Documents/103/NSI/notebook/P - Fil rouge Harry Potter/nsi-master-Projets-3 - Projet Choixpeau magique/Projets/3 - Projet Choixpeau magique/Characters.csv", mode='r', encoding='utf-8') as f:
    reader = csv.DictReader(f, delimiter=';')
    table_perso = [{key : value for key, value in element.items()} for element in reader]

with open("file://srv-dc01.d0440021j.fr/Perso/ELEVES/1ERE3/ABRASSART/Documents/103/NSI/notebook/P - Fil rouge Harry Potter/nsi-master-Projets-3 - Projet Choixpeau magique/Projets/3 - Projet Choixpeau magique/Caracteristiques_des_persos.csv", mode='r', encoding='utf-8') as f:
    reader = csv.DictReader(f, delimiter=';')
    table_carac = [{key : value for key, value in element.items()} for element in reader]

print(table_perso)
print(table_carac)


for element in table_perso:
    if element['Name'] in table_carac:
        table_carac[element['Name']].update({'id' : element['id']})
        table_carac[element['Name']].update({'House' : element['House']})
print(table_carac)